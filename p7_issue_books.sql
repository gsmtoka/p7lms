-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2019 at 07:07 AM
-- Server version: 8.0.13-4
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ycDMuoXQb5`
--

-- --------------------------------------------------------

--
-- Table structure for table `p7_issue_books`
--

CREATE TABLE `p7_issue_books` (
  `id` int(5) NOT NULL,
  `student_enrollment` varchar(50) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `student_sem` varchar(50) NOT NULL,
  `student_contact` varchar(50) NOT NULL,
  `student_email` varchar(50) NOT NULL,
  `books_name` varchar(50) NOT NULL,
  `books_issue_date` varchar(50) NOT NULL,
  `books_return_date` varchar(50) NOT NULL,
  `student_username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `p7_issue_books`
--

INSERT INTO `p7_issue_books` (`id`, `student_enrollment`, `student_name`, `student_sem`, `student_contact`, `student_email`, `books_name`, `books_issue_date`, `books_return_date`, `student_username`) VALUES
(3, '2', 'Lazare Khutsishvili', '3', '557474507', 'gsmwork2015@gmail.com', 'Photography', '12-09-2019', '13-09-2019', 'lazare'),
(4, '2', 'Lazare Khutsishvili', '3', '557474507', 'gsmwork2015@gmail.com', 'Tornikes Tskhovreba', '13-09-2019', '', 'lazare'),
(5, '7777777777', 'uzna uzna', '444', '555', 'ser_toko@yahoo.com', 'Lion King', '13-09-2019', '14-09-2019', 'uzna'),
(6, '7777777777', 'uzna uzna', '444', '555', 'ser_toko@yahoo.com', 'Udiplomo Future Sun InLow', '14-09-2019', '', 'uzna');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p7_issue_books`
--
ALTER TABLE `p7_issue_books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p7_issue_books`
--
ALTER TABLE `p7_issue_books`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
