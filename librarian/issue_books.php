<?php
session_start();
if(!isset($_SESSION["librarian"]))
{
    ?>

    <script type="text/javascript">
        window.location="login.php";
    </script>

    <?php
}

include "connection.php";
include "header.php";
?>

        <!-- page content area main -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3></h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Issue Books</h2>
                                
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                            
                            <form name="form1" action="" method="POST">
                                <table>
                                    <tr>
                                        <td>
                                            <select name="enr" class="form-control selectpicker">
                                                
                                                <?php
                                                $res = mysqli_query ($link, "SELECT enrollment FROM p7_student_registration");
                                                while ($row = mysqli_fetch_array ($res))
                                                {
                                                    echo "<option>";
                                                    echo $row["enrollment"];
                                                    echo "</option>";
                                                }
                                                ?>

                                            </select>
                                        </td>
                                        <td>
                                            <input type="submit" value="search" name="submit1" 
                                                class="form control btn btn-default" style="margin-top: 5px;">
                                        </td>
                                    </tr>
                                </table>

                                <?php
                                if (isset($_POST["submit1"])) {

                                    $res5=mysqli_query($link, "SELECT * FROM p7_student_registration WHERE enrollment='$_POST[enr]'");
                                    while($row5=mysqli_fetch_array($res5))
                                    {
                                        $firstname=$row5["firstname"];
                                        $lastname=$row5["lastname"];
                                        $username=$row5["username"];
                                        $email=$row5["email"];
                                        $contact=$row5["contact"];
                                        $sem=$row5["sem"];
                                        $enrollment=$row5["enrollment"];
                                        $_SESSION["enrollment"]=$enrollment;
                                        $_SESSION["username"]=$username;
                                    }

                                    ?>

                                    <table class="table table-bordered">
                                        <tr>
                                            <td><input type="text" class="form-control" placeholder="Enrollment No" 
                                                name="enrollment" value="<?php echo $enrollment; ?>" disabled></td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" placeholder="Student Name" 
                                                name="student_name" value="<?php echo $firstname.' '.$lastname ?>" required></td>
                                        </tr>  
                                        <tr>
                                            <td><input type="text" class="form-control" placeholder="Student Sem" 
                                                name="student_sem" value="<?php echo $sem; ?>" required></td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" placeholder="Student Contact" 
                                                name="student_contact" value="<?php echo $contact; ?>" required></td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" placeholder="Student Email" 
                                                name="student_email" value="<?php echo $email; ?>" required></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select name="books_name" class="form-control selectpicker">

                                                <?php
                                                $res = mysqli_query($link, "SELECT books_name FROM p7_add_books");
                                                while($row=mysqli_fetch_array($res))
                                                {
                                                    echo "<option>";
                                                    echo $row["books_name"];
                                                    echo "</option>";
                                                }
                                                ?>

                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" placeholder="Books Issue Date" 
                                                name="books_issue_date" value="<?php echo date("d-m-Y"); ?>" required></td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" class="form-control" placeholder="Student Username" 
                                                name="student_username" value="<?php echo $username; ?>" disabled></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" value="Issue Books" name="submit2" 
                                                    class="form-control btn btn-default" 
                                                    style="background-color: #304C57; color:white">
                                            </td>
                                        </tr>

                                    </table>

                                    <?php
                                }
                                ?>

                            </form>

                            <?php
                            if(isset($_POST["submit2"]))
                            {
                                
                                $qty=0;
                                $res=mysqli_query($link, "SELECT * FROM p7_add_books WHERE books_name='$_POST[books_name]'");
                                while($row=mysqli_fetch_array($res))
                                {
                                    $qty=$row["available_qty"];
                                }

                                if($qty==0)
                                {
                                    ?>
                                        <div class="alert alert-danger col-lg-6 col-lg-push-3">
                                        <strong style="color:white">This books are not available in stock.</strong>
                                        </div>

                                    <?php
                                }

                                else {
                                    
                                    mysqli_query($link, "INSERT INTO p7_issue_books (student_enrollment, student_name, student_sem, student_contact,
                                        student_email, books_name, books_issue_date, books_return_date, student_username)
                                     VALUES ('$_SESSION[enrollment]', '$_POST[student_name]', 
                                        '$_POST[student_sem]', '$_POST[student_contact]', '$_POST[student_email]', '$_POST[books_name]', 
                                        '$_POST[books_issue_date]', '', '$_SESSION[username]')");


                                    mysqli_query($link, "UPDATE p7_add_books SET available_qty=available_qty-1 WHERE books_name='$_POST[books_name]'");

                                    ?>

                                    <script type="text/javascript">
                                        alert("Books Issued Successfully");
                                        window.location.href=window.location.href;
                                    </script>

                                    <?php
                                }
                            } 
                            ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

<?php
include "footer.php";
?>