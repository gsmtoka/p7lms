-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2019 at 07:07 AM
-- Server version: 8.0.13-4
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ycDMuoXQb5`
--

-- --------------------------------------------------------

--
-- Table structure for table `p7_add_books`
--

CREATE TABLE `p7_add_books` (
  `id` int(5) NOT NULL,
  `books_name` varchar(50) NOT NULL,
  `books_image` varchar(500) NOT NULL,
  `books_author_name` varchar(50) NOT NULL,
  `books_publication_name` varchar(50) NOT NULL,
  `books_purchase_date` varchar(50) NOT NULL,
  `books_price` varchar(50) NOT NULL,
  `books_qty` varchar(50) NOT NULL,
  `available_qty` varchar(50) NOT NULL,
  `librarian_username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `p7_add_books`
--

INSERT INTO `p7_add_books` (`id`, `books_name`, `books_image`, `books_author_name`, `books_publication_name`, `books_purchase_date`, `books_price`, `books_qty`, `available_qty`, `librarian_username`) VALUES
(2, 'Lion King', 'books_image/f444ca3486299d965ae5cac28e412cdblionking-920x584.jpg', 'zooparkis gamge', 'zooparki', '12.09.2019', '20', '20', '19', 'barbare'),
(3, 'Php', 'books_image/be815c64f48c0218db422f558da704bcPHP.jpg', 'Valeri', 'Learning PHP', '12.09.2019', '30', '30', '30', 'barbare'),
(4, 'Photography', 'books_image/dfe6b0131c8927a05ce063325ee68909stock-photo-159358357.jpg', 'Giorgi', 'Photography', '12.09.2019', '40', '40', '41', 'barbare'),
(5, 'Tornikes Tskhovreba', 'books_image/d9659c68fc253f79431ef74dedecf16e65675903_396598660957929_6989856427229577216_n.png', 'Tornike Khutsishvili', 'Tornikes Life', '12.09.2019', '50', '50', '49', 'barbare'),
(7, 'ciyvi', 'books_image/eab1e989485575ae6432a95e1d7b2e30ciyvi.jpg', 'zooparkis gamge', 'zooparki', '11.09.2019', '10', '10', '10', 'barbare'),
(8, 'Udiplomo Future Sun InLow', 'books_image/b8c7500f17e38091db2b57a28fd705cd142631868177.jpg', 'Tornike', 'Kino', '14.09.2019', '25', '25', '24', 'jemala');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p7_add_books`
--
ALTER TABLE `p7_add_books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p7_add_books`
--
ALTER TABLE `p7_add_books`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
