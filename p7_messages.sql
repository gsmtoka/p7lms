-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2019 at 07:08 AM
-- Server version: 8.0.13-4
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ycDMuoXQb5`
--

-- --------------------------------------------------------

--
-- Table structure for table `p7_messages`
--

CREATE TABLE `p7_messages` (
  `id` int(5) NOT NULL,
  `susername` varchar(50) NOT NULL,
  `dusername` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `msg` varchar(500) NOT NULL,
  `read1` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `p7_messages`
--

INSERT INTO `p7_messages` (`id`, `susername`, `dusername`, `title`, `msg`, `read1`) VALUES
(1, 'barbare', 'gsmtoka', 'hello', 'rogor xar?', 'y'),
(2, 'barbare', 'gsmtoka', 'hello', 'axla rogor xar?', 'y'),
(3, 'barbare', 'gsmtoka', 'hame', 'opiuyiukyoulo;iyjokiuli lh ;kjh lkjg liug ilkug lku ', 'y'),
(4, 'barbare', 'uzna', 'salami', 'dzmao', 'y'),
(5, 'barbare', 'uzna', 'meore', 'meore', 'y'),
(6, 'jemala', 'uzna', ';)', ':) :) :)', 'y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p7_messages`
--
ALTER TABLE `p7_messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p7_messages`
--
ALTER TABLE `p7_messages`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
